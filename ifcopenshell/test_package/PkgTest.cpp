/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>
#include <vector>

#include <ifcparse/IfcFile.h>
#include <ifcparse/Ifc2x3.h>
#include <ifcparse/Ifc4.h>
#include <ifcparse/IfcHierarchyHelper.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'IFCOpenShell' package test (compilation, linking, and execution).\n";

    IfcHierarchyHelper<Ifc2x3> ifc2x3_file;
    IfcHierarchyHelper<Ifc4> ifc4_file;

	ifc2x3_file.header().file_name().name("Test_IFC2x3.ifc");
    ifc4_file.header().file_name().name("Test_IFC4.ifc");

    std::cout << "'IFCOpenShell' package works!" << std::endl;
    return EXIT_SUCCESS;
}
