#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile


class IfcOpenShell(ConanFile):
    name = "ifcopenshell"
    version = "0.6.0"
    license = "LGPL-3.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-ifcopenshell"
    description = "The open-source IFC library and geometry engine."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "http://ifcopenshell.org/"

    _src_dir = "ifcopenshell_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "fPIC=True"

    exports = "../LICENCE.md"

    requires = "opencascade/7.3.0@bimandscan/stable", \
               "boost_thread/1.69.0@bincrafters/stable", \
               "boost_system/1.69.0@bincrafters/stable", \
               "boost_regex/1.69.0@bincrafters/stable", \
               "boost_date_time/1.69.0@bincrafters/stable", \
               "boost_program_options/1.69.0@bincrafters/stable", \
               "boost_regex/1.69.0@bincrafters/stable", \
               "boost_chrono/1.69.0@bincrafters/stable", \
               "boost_atomic/1.69.0@bincrafters/stable", \
               "icu/62.1@bincrafters/stable", \
               "boost_dynamic_bitset/1.69.0@bincrafters/stable", \
               "boost_container_hash/1.69.0@bincrafters/stable", \
               "boost_uuid/1.69.0@bincrafters/stable", \
               "boost_circular_buffer/1.69.0@bincrafters/stable", \
               "boost_property_tree/1.69.0@bincrafters/stable"

    build_requires = "cmake_findboost_modular/1.69.0@bincrafters/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        git_uri = "https://github.com/IfcOpenShell/IfcOpenShell.git"
        src_cmakefile = f"{self._src_dir}/cmake/CMakeLists.txt"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "v0.6.0")
        git.checkout(element = "6a5bfefce91a1642bdf26f0443274bcb8a616010") # commit hash -> 20/1/2019

        # Patch CMake config:
        tools.replace_in_file(src_cmakefile,
                              "project (IfcOpenShell)",
                              "project(IfcOpenShell CXX)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup(TARGETS)\n")

        # Fix TBB-enabled OpenCASCADE and ICU compilation:
        tools.replace_in_file(src_cmakefile,
                              "TARGET_LINK_LIBRARIES(IfcGeom_ifc2x3 IfcParse ${OPENCASCADE_LIBRARIES})",
                              "target_link_libraries(IfcGeom_ifc2x3 PUBLIC IfcParse ${OPENCASCADE_LIBRARIES} CONAN_PKG::opencascade CONAN_PKG::icu)\n")

        tools.replace_in_file(src_cmakefile,
                              "TARGET_LINK_LIBRARIES(IfcGeom_ifc4 IfcParse ${OPENCASCADE_LIBRARIES})",
                              "target_link_libraries(IfcGeom_ifc4 PUBLIC IfcParse ${OPENCASCADE_LIBRARIES} CONAN_PKG::opencascade CONAN_PKG::icu)\n")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = True # force shared build
        cmake.definitions["ENABLE_BUILD_OPTIMIZATIONS"] = (self.settings.build_type == "Release")
        cmake.definitions["IFCCONVERT_DOUBLE_PRECISION"] = True
        cmake.definitions["USE_VLD"] = False
        cmake.definitions["USE_MMAP"] = False
        cmake.definitions["BUILD_EXAMPLES"] = False
        cmake.definitions["BUILD_IFCMAX"] = False
        cmake.definitions["BUILD_IFCPYTHON"] = False
        cmake.definitions["COLLADA_SUPPORT"] = False
        cmake.definitions["UNICODE_SUPPORT"] = True
        cmake.definitions["IFCXML_SUPPORT"] = False
        cmake.definitions["BUILD_GEOMSERVER"] = False
        cmake.definitions["BUILD_CONVERT"] = False

        # RPATH fix:
        cmake.definitions["CMAKE_SKIP_BUILD_RPATH"] = True

        # Manually-add libraries to link with:
        cmake.definitions["OCC_INCLUDE_DIR"] = self.deps_cpp_info["opencascade"].include_paths[0]
        cmake.definitions["OCC_LIBRARY_DIR"] = self.deps_cpp_info["opencascade"].lib_paths[0]
        cmake.definitions["ICU_INCLUDE_DIR"] = self.deps_cpp_info["icu"].include_paths[0]
        cmake.definitions["ICU_LIBRARY_DIR"] = self.deps_cpp_info["icu"].lib_paths[0]

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = f"{self._src_dir}/cmake")
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("COPYING",
                  "licenses",
                  self._src_dir)

        self.copy("COPYING.LESSER",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os == "Linux":
            self.cpp_info.libs.extend([
                                          "rt",
                                          "dl"
                                      ])

            if self.settings.compiler == "gcc":

                self.cpp_info.libs.append("m")
