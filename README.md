# BIM & Scan� Third-Party Library (IFCOpenShell)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for [IFCOpenShell](http://ifcopenshell.org/), the open-source IFC library and geometry engine.

The 'IfcConvert' and 'IfcGeomServer' tools are not packaged with this build!

Supports version 0.6.0 (unstable, from Git repo).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) and [BIM & Scan� (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repositories for third-party dependencies.
