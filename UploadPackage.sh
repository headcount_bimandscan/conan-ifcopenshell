#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "ifcopenshell/0.6.0@bimandscan/unstable"
